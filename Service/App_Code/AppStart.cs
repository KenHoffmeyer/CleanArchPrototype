﻿using AutofacIntegration = Autofac.Integration.Wcf;
using Autofac;

namespace Service
{
    public class AppStart
    {
        public static void AppInitialize()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<ServiceAutofacModule>();
            builder.RegisterModule<Bpc.BpcAutofacModule>();
            
            AutofacIntegration.AutofacHostFactory.Container = builder.Build();
        }
    }
}