﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Bpc.Checklist;
using Bpc.Contracts.Checklist;
using Service.Persistence.Checklist;
using Service.Persistence.Users;
using Service.Users;

namespace Service.Persistence
{
    public class ServicePersistenceAutoMapperProfile : Profile
    {
        public ServicePersistenceAutoMapperProfile(
            Func<int, string, IQuestion> questionFactoryFunc,
            Func<int, bool?, string, IAnswer> answerFactoryFunc,
            Func<int, IReadOnlyList<IQuestion>, IChecklist> checklistFactoryFunc)
        {
            CreateMap<UserEntity, User>();

            AllowNullDestinationValues = false;

            CreateMap<AnswerEntity, IAnswer>()
                .ConstructUsing(entity =>
                {
                    var answer = answerFactoryFunc(entity.Id, entity.Yes, entity.Notes);
                    return answer;
                })
                .ReverseMap();
            
            CreateMap<QuestionEntity, IQuestion>()
                .ConstructUsing(entity =>
                {
                    var question = questionFactoryFunc(entity.Order, entity.Text);
                    return question;
                });

            CreateMap<ChecklistEntity, IChecklist>()
                .ConstructUsing((entity, context) => 
                    checklistFactoryFunc(entity.Id, context.Mapper.Map<IReadOnlyList<IQuestion>>(entity.Questions)));
        }
    }
}