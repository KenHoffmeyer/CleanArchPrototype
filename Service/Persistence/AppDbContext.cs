﻿using Microsoft.EntityFrameworkCore;
using Service.Persistence.Checklist;
using Service.Persistence.Users;

namespace Service.Persistence
{
    class AppDbContext : DbContext
    {
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<ChecklistEntity> Checklists { get; set; }
        public DbSet<QuestionEntity> Questions { get; set; }
        public DbSet<AnswerEntity> Answers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase();
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ChecklistEntity>()
                .HasMany(ce => ce.Questions)
                .WithOne(q => q.Checklist);

            modelBuilder.Entity<QuestionEntity>()
                .HasOne(q => q.Answer)
                .WithOne(a => a.Question);
        }
    }
}