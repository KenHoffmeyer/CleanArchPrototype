﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Service.Persistence.Checklist
{
    class QuestionEntity
    {
        public int Id { get; set; }
        public int Order { get; set; }

        public string Text { get; set; }

        [ForeignKey("AnswerEntity")]
        public AnswerEntity Answer { get; set; } = new AnswerEntity();

        public ChecklistEntity Checklist { get; set; }


    }
}