﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Service.Persistence.Users;

namespace Service.Persistence.Checklist
{
    class ChecklistEntity
    {
        public int Id { get; set; }
        public UserEntity User { get; set; }
        public IEnumerable<QuestionEntity> Questions { get; set; } = new List<QuestionEntity>();
    }
}