﻿using System.Linq;
using Service.DataContracts;

namespace Service
{
    public partial class BpcService
    {
        /// <summary>
        /// Checklist stuff for BpcService goes here.
        /// </summary>
        public ChecklistDto GetChecklistForUser(int userId)
        {
            var user = _userProcessor.GetUsers().FirstOrDefault(u => u.Id == userId);
            var raw = _checklistProcessor.GetChecklistForUser(user);

            var output = _mapper.Map<ChecklistDto>(raw);

            return output;
        }

        public ChecklistDto CreateChecklistForUser(int userId)
        {
            return new ChecklistDto
            {
                Id = 2,
                Questions = new[] { new QuestionDto { Text = "The NEW question" } }
            };
        }

        public AnswerDto SaveAnswer(int userId, int answerId, bool? yes, string notes)
        {
            var user = _userProcessor.GetUsers().FirstOrDefault(u => u.Id == userId);
            var raw = _checklistProcessor.SaveAnswer(user, answerId, yes, notes);

            var output = _mapper.Map<AnswerDto>(raw);
            return output;
        }
    }
}