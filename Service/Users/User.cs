﻿using System.Diagnostics;
using Bpc.Contracts.Users;
using Bpc.Users;

namespace Service.Users
{
    [DebuggerDisplay("{Id} {Name} {CanCreateBpc}")]
    class User : IUser
    {
        public int Id { get; }
        public string Name { get; }
        public bool CanCreateBpc { get; }

        public User(int id, string name, bool canCreateBpc)
        {
            Id = id;
            Name = name;
            CanCreateBpc = canCreateBpc;
        }
    }
}