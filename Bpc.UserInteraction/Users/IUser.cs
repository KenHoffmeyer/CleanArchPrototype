﻿namespace Bpc.UserInteraction.Users
{
    public interface IUser
    {
        bool CanCreateBpc { get; set; }
        int Id { get; set; }
        string Name { get; set; }
    }
}
