namespace Bpc.UserInteraction.Contracts.Checklist
{
    public interface IQuestion
    {
        int Order { get; }

        string Text { get; }
        
        IAnswer Answer { get; set; }
    }
}