using Bpc.UserInteraction.Users;

namespace Bpc.UserInteraction.Contracts.Checklist
{
    public interface IChecklistPersistor
    {
        IChecklist GetChecklistForUser(IUser selectedUser);
        bool SaveAnswer(IUser user, IAnswer answer);
    }
}