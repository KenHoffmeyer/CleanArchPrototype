using System.Collections.Generic;

namespace Bpc.UserInteraction.Contracts.Checklist
{
    public interface IChecklist
    {
        IReadOnlyList<IQuestion> Questions { get; set; }
    }
}