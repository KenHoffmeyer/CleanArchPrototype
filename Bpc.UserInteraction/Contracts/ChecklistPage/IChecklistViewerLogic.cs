﻿using System.ComponentModel;
using Bpc.UserInteraction.Contracts.Checklist;
using Bpc.UserInteraction.Users;

namespace Bpc.UserInteraction.Contracts.ChecklistPage
{
    public interface IChecklistViewerLogic : INotifyPropertyChanged
    {
        IUser SelectedUser { get; set; }
        IChecklist CurrentChecklist { get; }
        bool IsInCreateCommandOptionMode { get; }
        bool IsQuestionListVisible { get; }
        string Title { get; set; }
    }
}