﻿using System.ComponentModel;

namespace Bpc.UserInteraction.Contracts.Common
{
    interface ISelectedItemBag<T> : INotifyPropertyChanged
    {
        T SelectedItem { get; set; }
    }
}
