﻿using Autofac;
using Bpc.UserInteraction.Checklist;
using Bpc.UserInteraction.ChecklistPage;
using Bpc.UserInteraction.Common;
using Bpc.UserInteraction.Common.SelectedItem;
using Bpc.UserInteraction.Contracts.Checklist;
using Bpc.UserInteraction.Contracts.Common;

namespace Bpc.UserInteraction
{
    public class BpcUserInteractionAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterModule<BpcUserInteractionChecklistPageAutofacModule>();

            builder.RegisterType<UsersModel>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<ChecklistLogic>().AsImplementedInterfaces().InstancePerLifetimeScope();


            builder.RegisterGeneric(typeof(SelectedItemBag<>))
                .As(typeof(ISelectedItemBag<>))
                .InstancePerLifetimeScope();


            builder.RegisterType<Checklist.Checklist>().AsImplementedInterfaces();
            builder.RegisterType<Question>().AsImplementedInterfaces();
            builder.RegisterType<Answer>().AsImplementedInterfaces();
        }
    }
}
