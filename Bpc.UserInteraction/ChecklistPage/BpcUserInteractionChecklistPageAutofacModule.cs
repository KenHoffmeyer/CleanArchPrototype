﻿using Autofac;

namespace Bpc.UserInteraction.ChecklistPage
{
    class BpcUserInteractionChecklistPageAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<ChecklistPageLogic>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<ChecklistViewerLogic>().AsImplementedInterfaces().InstancePerLifetimeScope();
        }
    }
}
