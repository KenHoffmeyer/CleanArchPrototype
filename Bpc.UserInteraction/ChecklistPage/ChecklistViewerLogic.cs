﻿using System.ComponentModel;
using System.Linq;
using Bpc.UserInteraction.Contracts.Checklist;
using Bpc.UserInteraction.Contracts.ChecklistPage;
using Bpc.UserInteraction.Contracts.Common;
using Bpc.UserInteraction.Users;
using PropertyChanged;

namespace Bpc.UserInteraction.ChecklistPage
{
    [ImplementPropertyChanged]
    internal class ChecklistViewerLogic : IChecklistViewerLogic
    {
        private readonly IChecklistLogic _checklistLogic;
        private readonly ISelectedItemBag<IUser> _selectedUserBag;

        public string Title { get; set; }
        public IUser SelectedUser { get; set; }
        public IChecklist CurrentChecklist { get; set; }
        public bool IsInCreateCommandOptionMode => CurrentChecklist == null;
        public bool IsQuestionListVisible => CurrentChecklist != null;

        public ChecklistViewerLogic(IChecklistLogic checklistLogic, ISelectedItemBag<IUser> selectedUserBag)
        {
            _checklistLogic = checklistLogic;
            _selectedUserBag = selectedUserBag;
            _selectedUserBag.PropertyChanged += (sender, args) => SyncWithSelectedUserBag();
            SyncWithSelectedUserBag();
        }

        // Called by Fody
        // ReSharper disable once UnusedMember.Local
        private void OnSelectedUserChanged()
        {
            var newSelectedUser = SelectedUser;
            _selectedUserBag.SelectedItem = newSelectedUser;
            Title = $"Current User = {newSelectedUser?.Name}";

            // get the checklist for the user
            var newChecklist = _checklistLogic.GetChecklistForUser(newSelectedUser);
            SwitchChecklist(newChecklist);
        }

        private void SwitchChecklist(IChecklist newChecklist)
        {
            // unwire old checklist's answers
            var current = CurrentChecklist;
            if (current != null)
            {
                foreach (var answer in current.Questions.Select(q => q.Answer).Where(a => a != null))
                {
                    answer.PropertyChanged -= OnAnswerChanged;
                }
            }

            // wire new checklist's answers
            if (newChecklist != null)
            {
                foreach (var answer in newChecklist.Questions.Select(q => q.Answer).Where(a => a != null))
                {
                    answer.PropertyChanged += OnAnswerChanged;
                }
            }

            CurrentChecklist = newChecklist;
        }

        private void OnAnswerChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(IAnswer.Yes):
                case nameof(IAnswer.Notes):
                    SaveAnswer(sender as IAnswer);
                    break;
            }
        }

        private void SaveAnswer(IAnswer answer)
        {
            _checklistLogic.SaveAnswer(SelectedUser, answer);
        }

        private void SyncWithSelectedUserBag()
        {
            var selectedUserFromBag = _selectedUserBag.SelectedItem;
            SelectedUser = selectedUserFromBag;
        }

        // Called by Fody.
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
