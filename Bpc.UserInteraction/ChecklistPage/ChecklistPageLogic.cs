﻿using System.Collections.Generic;
using System.ComponentModel;
using Bpc.UserInteraction.Common;
using Bpc.UserInteraction.Contracts.Checklist;
using Bpc.UserInteraction.Contracts.ChecklistPage;
using Bpc.UserInteraction.Contracts.Common;
using Bpc.UserInteraction.Users;
using PropertyChanged;

namespace Bpc.UserInteraction.ChecklistPage
{
    [ImplementPropertyChanged]
    internal class ChecklistPageLogic : IChecklistPageLogic
    {
        private readonly IUsersModel _usersModel;
        private readonly IChecklistLogic _checklistLogic;
        private readonly ISelectedItemBag<IUser> _selectedUserBag;

        public IUser SelectedUser { get; set; }
        public bool IsUserSelectorActive { get; private set; }
        public IList<IUser> UserSelection { get; } = new List<IUser>();
        public IChecklist Checklist { get; set; }

        public bool IsChecklistViewerActive { get; set; }

        public ChecklistPageLogic(IUsersModel usersModel, IChecklistLogic checklistLogic, ISelectedItemBag<IUser> selectedUserBag)
        {
            _usersModel = usersModel;
            _usersModel.CollectionChanged += (sender, args) => SyncUsers();
            _checklistLogic = checklistLogic;
            _selectedUserBag = selectedUserBag;
            _selectedUserBag.PropertyChanged += (sender, args) => SyncWithSelectedUserBag();
            SyncWithSelectedUserBag();
        }

        private void SyncUsers()
        {
            UserSelection.Clear();
            
            foreach (var user in _usersModel)
            {
                UserSelection.Add(user);
            }
        }

        public void Load()
        {
            _usersModel.Load();
            SyncUsers();
            IsUserSelectorActive = UserSelection.Count > 0;
        }
        
        // ReSharper disable once UnusedMember.Local
        // called by fody
        private void OnSelectedUserChanged()
        {
            var newSelectedUser = SelectedUser;
            IsChecklistViewerActive = newSelectedUser?.CanCreateBpc ?? false;
            Checklist = _checklistLogic.GetChecklistForUser(newSelectedUser);
            _selectedUserBag.SelectedItem = newSelectedUser;
        }


        private void SyncWithSelectedUserBag()
        {
            var selectedUserFromBag = _selectedUserBag.SelectedItem;
            SelectedUser = selectedUserFromBag;
        }

        // Gets implemented fully by Fody.
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
