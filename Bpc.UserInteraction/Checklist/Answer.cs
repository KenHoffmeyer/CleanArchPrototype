using System.ComponentModel;
using Bpc.UserInteraction.Contracts.Checklist;
using PropertyChanged;

namespace Bpc.UserInteraction.Checklist
{
    [ImplementPropertyChanged]
    internal class Answer : IAnswer
    {
        public Answer(bool? yes, string notes)
        {
            Yes = yes;
            Notes = notes;
        }

        public int Id { get; set; }
        public bool? Yes { get; set; }
        public string Notes { get; set; }
        public bool IsNotesEnabled => !Yes ?? false;

        #region Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}