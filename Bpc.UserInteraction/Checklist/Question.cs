using Bpc.UserInteraction.Contracts.Checklist;

namespace Bpc.UserInteraction.Checklist
{
    internal class Question : IQuestion
    {
        public Question(int order, string text)
        {
            Order = order;
            Text = text;
        }

        public int Order { get; set; }
        public string Text { get; set; }

        public IAnswer Answer { get; set; }
    }
}