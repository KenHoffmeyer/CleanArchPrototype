﻿using Bpc.Contracts.EvalEngine.Info;

namespace Bpc.Tests.EvalEngine.Mocks
{
    internal class ConditionInfo : IConditionInfo
    {
        public IExprInfo<bool> Expr { get; set; }        
    }
}
