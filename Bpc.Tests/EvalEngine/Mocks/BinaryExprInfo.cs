﻿using System;
using Bpc.Contracts.EvalEngine.Info;

namespace Bpc.Tests.EvalEngine.Mocks
{
    internal class BinaryExprInfo<T> : IBinaryExprInfo<T>
    {
        public IExprInfo LeftExpr { get; set; }
        public IExprInfo RightExpr { get; set; }
        public Operator Operator { get; set; }

        public Type ResultType => typeof(T);

    }
}