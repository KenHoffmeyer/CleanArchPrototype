﻿Feature: Condition
	A condition evaluates its expression to a boolean

Scenario Outline: Condition with (in)equality expressions
Given a condition with this expression: <left value> <operator> <right value>
When condition evaluates
Then the condition return <expected result>
Examples: 
 | left value | operator | right value | expected result |
 | this value | =        | this value  | true            |
 | this value | =        | that value  | false           |
 | this value | !=       | this value  | false           |
 | this value | !=       | that value  | true            |
