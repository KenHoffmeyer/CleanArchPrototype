using System.Collections.Generic;
using Bpc.Contracts.Checklist;

namespace Bpc.Tests.Checklist.Mocks
{
    internal class ChecklistMock : IChecklist
    {
        public int Id { get; }
        public IReadOnlyList<IQuestion> Questions { get; }
        
        public ChecklistMock(int id, IReadOnlyList<IQuestion> questions)
        {
            Id = id;
            Questions = questions;
        }
    }
}