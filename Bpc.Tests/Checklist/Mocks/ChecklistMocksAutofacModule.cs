﻿using Autofac;

namespace Bpc.Tests.Checklist.Mocks
{
    internal class ChecklistMocksAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            var typesToRegister = new[]
            {
                typeof(AnswerMock),
                typeof(ChecklistMock),
                typeof(QuestionMock)
            };

            foreach (var type in typesToRegister)
            {
                builder.RegisterType(type).AsImplementedInterfaces().InstancePerLifetimeScope();
            }
        }
    }
}
