using Bpc.Contracts.Checklist;

namespace Bpc.Tests.Checklist.Mocks
{
    internal class AnswerMock : IAnswer
    {
        public AnswerMock(int id, bool? yes, string notes)
        {
            Id = id;
            Yes = yes;
            Notes = notes;
        }

        public int Id { get; }
        public bool? Yes { get; }
        public string Notes { get; }
    }
}