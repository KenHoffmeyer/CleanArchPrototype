using System;
using Bpc.Contracts.Checklist;
using Bpc.Contracts.Users;

namespace Bpc.Checklist
{
    internal class AnswerCrudLogic : IAnswerCrudLogic
    {
        private readonly IChecklistPersistor _checklistPersistor;
        private readonly Func<int, bool?, string, IAnswer> _answerFactoryFunc;

        public AnswerCrudLogic(IChecklistPersistor checklistPersistor, Func<int, bool?, string, IAnswer> answerFactoryFunc)
        {
            _checklistPersistor = checklistPersistor;
            _answerFactoryFunc = answerFactoryFunc;
        }

        public IAnswer SaveAnswer(IUser user, int answerId, bool? yes, string notes)
        {
            IAnswer output;
            // get old answer
            var answer = _checklistPersistor.LoadAnswer(user, answerId);

            // can't find it? return null
            if (answer == null)
            {
                output = null;
            }
            else
            {
                var newAnswer = _answerFactoryFunc.Invoke(answer.Id, yes, notes);

                // save
                _checklistPersistor.SaveAnswer(user, newAnswer);
                output = newAnswer;
            }

            // return updated answer (or null)
            return output;
        }
    }
}