﻿using Autofac;
using Bpc.Checklist.Generator;

namespace Bpc.Checklist
{
    internal class BpcChecklistAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<QuestionGenerator>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<ChecklistProcessor>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<AnswerCrudLogic>().AsImplementedInterfaces().InstancePerLifetimeScope();

        }
    }
}
