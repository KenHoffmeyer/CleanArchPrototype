﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bpc.Checklist.Generator.Template
{
    public interface IQuestionTemplate
    {
        int Id { get; } // not sure if this is needed here.
        IQuestionText Text { get; }
    }

    public interface IQuestionText
    {
        int Id { get; }
        string Text { get; }
    }
}
