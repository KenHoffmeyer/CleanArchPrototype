﻿using Bpc.Contracts.Checklist;
using Bpc.Contracts.Users;

namespace Bpc.Checklist
{
    internal interface IAnswerCrudLogic
    {
        IAnswer SaveAnswer(IUser user, int answerId, bool? yes, string notes);
    }
}