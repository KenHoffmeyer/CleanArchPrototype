using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Bpc.Checklist.Generator;
using Bpc.Contracts.Checklist;
using Bpc.Contracts.Users;

namespace Bpc.Checklist
{
    internal class ChecklistProcessor : IChecklistProcessor
    {
        private readonly IQuestionGenerator _questionGenerator;
        private readonly Func<int, IReadOnlyList<IQuestion>, IChecklist> _checklistFactory;
        private readonly IChecklistPersistor _checklistPersistor;
        private readonly IAnswerCrudLogic _answerCrudLogic;
        private readonly IDictionary<IUser, IChecklist> _checklists = new ConcurrentDictionary<IUser, IChecklist>();

        public ChecklistProcessor(
            IQuestionGenerator questionGenerator,
            Func<int, IReadOnlyList<IQuestion>, IChecklist> checklistFactory,
            IChecklistPersistor checklistPersistor,
            IAnswerCrudLogic answerCrudLogic)
        {
            _questionGenerator = questionGenerator;
            _checklistFactory = checklistFactory;
            _checklistPersistor = checklistPersistor;
            _answerCrudLogic = answerCrudLogic;
        }

        public IChecklist GetChecklistForUser(IUser user)
        {
            IChecklist output;

            if (user == null)
            {
                output = null;
            }
            else
            {
                if (!_checklists.TryGetValue(user, out var checklist))
                {
                    checklist = _checklistPersistor.Load(user);

                    if (checklist != null)
                    {
                        _checklists[user] = checklist;
                    }
                }
                output = checklist;
            }

            return output;
        }

        public IChecklist CreateChecklistForUser(IUser user)
        {
            if (_checklists.TryGetValue(user, out var existingChecklist))
            {
                throw new Exception($"Checklist already exist, with {existingChecklist.Questions.Count} question(s).");
            }

            var questions = _questionGenerator.GenerateQuestions(user.Name).ToList();
            var rawNewChecklist = _checklistFactory.Invoke(0, questions);

            // need to get new ID for the new checklist.
            var newChecklist = _checklistPersistor.Save(user, rawNewChecklist);

            if (newChecklist == null)
            {
                throw new Exception();
            }
            else
            {
                _checklists.Add(user, newChecklist);
            }

            return newChecklist;
        }

        public bool SaveChecklist(IUser user, IChecklist checklist)
        {
            throw new NotImplementedException();
        }

        public IAnswer SaveAnswer(IUser user, int answerId, bool? yes, string notes)
        {
            return _answerCrudLogic.SaveAnswer(user, answerId, yes, notes);
        }
    }
}