using System.Collections.Generic;
using Bpc.Contracts.Users;

namespace Bpc.Users
{
    internal class UserProcessor : IUserProcessor
    {
        private readonly IUserPersistor _userPersistor;

        public UserProcessor(IUserPersistor userPersistor)
        {
            _userPersistor = userPersistor;
        }

        public IEnumerable<IUser> GetUsers()
        {
            return _userPersistor.LoadUsers();
        }
    }
}