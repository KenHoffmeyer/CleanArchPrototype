﻿using Bpc.Contracts.EvalEngine;
using Bpc.Contracts.EvalEngine.Expr;

namespace Bpc.EvalEngine
{
    internal class Condition : ICondition
    {
        public IExpr<bool> Expr { get; }

        public Condition(IExpr<bool> expr)
        {
            Expr = expr;
        }

        public bool Evaluate(IContext context)
        {
            return Expr.Evaluate(context);
        }

        #region Implementation of IExpr

        object IExpr.Evaluate(IContext context)
        {
            return Evaluate(context);
        }

        #endregion
    }
}