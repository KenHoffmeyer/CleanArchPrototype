using System.Collections.Generic;
using System.Linq;
using Autofac;
using Bpc.Contracts.EvalEngine.Expr;
using Bpc.Contracts.EvalEngine.Info;

namespace Bpc.EvalEngine.Expr
{
    /// <summary>
    /// Creates a <see cref="IExpr"/> based on <see cref="IExprInfo"/>
    /// </summary>
    internal class ExprFactory : IExprFactory
    {
        private readonly IComponentContext _container;

        public ExprFactory(IComponentContext container)
        {
            _container = container;
        }

        public IExpr<T> Create<T>(IExprInfo<T> info)
        {
            var creators = _container.Resolve<IEnumerable<IExprCreator<T>>>();
            var creator = creators
                .First(c => c.CanHandle(info));

            var output = creator.Create(info);
            
            return output;
        }

        public IExpr CreateNoGeneric(IExprInfo info)
        {
            var exprFactory = GetType().GetMethod("Create")
                .MakeGenericMethod(info.ResultType);
            var expr = (IExpr) exprFactory.Invoke(this, new object[] { info });
            return expr;
        }
    }
}