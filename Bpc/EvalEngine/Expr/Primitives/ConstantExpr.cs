﻿using Bpc.Contracts.EvalEngine;
using Bpc.Contracts.EvalEngine.Expr;

namespace Bpc.EvalEngine.Expr.Primitives
{
    internal class ConstantExpr<T> : IExpr<T>
    {
        public T Value { get; set; }

        public ConstantExpr(T value)
        {
            Value = value;
        }

        public T Evaluate(IContext context)
        {
            return Value;
        }

        object IExpr.Evaluate(IContext context)
        {
            return Evaluate(context);
        }
    }
}