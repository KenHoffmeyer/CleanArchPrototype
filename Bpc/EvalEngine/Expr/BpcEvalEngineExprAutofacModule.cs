﻿using Autofac;
using Bpc.Contracts.EvalEngine.Expr;
using Bpc.EvalEngine.Expr.Evaluative;
using Bpc.EvalEngine.Expr.Primitives;

namespace Bpc.EvalEngine.Expr
{
    internal class BpcEvalEngineExprAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<Condition>().AsImplementedInterfaces().InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(ConstantExprCreator<>)).As(typeof(IExprCreator<>));
            builder.RegisterGeneric(typeof(EqualExprCreator<>)).As(typeof(IExprCreator<>));
            builder.RegisterGeneric(typeof(UnequalExprCreator<>)).As(typeof(IExprCreator<>));

            builder.RegisterGeneric(typeof(ConstantExpr<>));
            builder.RegisterType<EqualExpr>();
            builder.RegisterType<UnequalExpr>();

            builder.RegisterType<ExprFactory>().As<IExprFactory>();
        }
    }
}
