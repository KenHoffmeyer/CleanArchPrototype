﻿using System;
using Bpc.Contracts.EvalEngine;
using Bpc.Contracts.EvalEngine.Expr;

namespace Bpc.EvalEngine.Expr.Evaluative
{
    internal abstract class BinaryExpr : IExpr
    {
        private readonly Func<object, object, bool> _evaluateFunc;
        public IExpr LeftExpr { get; set; }
        public IExpr RightExpr { get; set; }

        protected BinaryExpr(Func<object, object, bool> evaluateFunc, IExpr leftExpr, IExpr rightExpr)
        {
            _evaluateFunc = evaluateFunc;
            LeftExpr = leftExpr;
            RightExpr = rightExpr;
        }

        public object Evaluate(IContext context)
        {
            var leftValue = LeftExpr.Evaluate(context);
            var rightValue = RightExpr.Evaluate(context);

            return _evaluateFunc(leftValue, rightValue);
        }
    }
}