using Bpc.Contracts.EvalEngine.Expr;
using Bpc.Contracts.EvalEngine.Info;

namespace Bpc.EvalEngine.Expr.Evaluative
{
    internal class UnequalExprCreator<T> : IExprCreator<T>
    {
        private readonly CreateUnequalExpr _unequalExprFactoryFunc;
        private readonly IExprFactory _exprFactory;

        public UnequalExprCreator(CreateUnequalExpr unequalExprFactoryFunc, IExprFactory exprFactory)
        {
            _unequalExprFactoryFunc = unequalExprFactoryFunc;
            _exprFactory = exprFactory;
        }

        public bool CanHandle(IExprInfo<T> info)
        {
            var binaryExprInfo = info as IBinaryExprInfo<bool>;
            return binaryExprInfo?.Operator == Operator.Unequal;
        }

        public IExpr<T> Create(IExprInfo<T> info)
        {
            var binaryExprInfo = (IBinaryExprInfo<bool>)info;

            var leftExpr = _exprFactory.CreateNoGeneric(binaryExprInfo.LeftExpr);
            var rightExpr = _exprFactory.CreateNoGeneric(binaryExprInfo.RightExpr);
            var output = _unequalExprFactoryFunc(leftExpr, rightExpr);
            
            return (IExpr<T>) output;
        }
    }
}