﻿using Autofac;

namespace Bpc
{
    public class BpcAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterModule<Checklist.BpcChecklistAutofacModule>();
            builder.RegisterModule<Users.BpcUsersAutofacModule>();
            builder.RegisterModule<EvalEngine.Expr.BpcEvalEngineExprAutofacModule>();
        }
    }
}
