using System.Collections.Generic;

namespace Bpc.Contracts.Checklist
{
    public interface IChecklist
    {
        int Id { get; }
        IReadOnlyList<IQuestion> Questions { get; }
    }
}