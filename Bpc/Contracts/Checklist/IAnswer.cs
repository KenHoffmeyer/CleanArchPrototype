namespace Bpc.Contracts.Checklist
{
    public interface IAnswer
    {
        int Id { get; }
        bool? Yes { get; }
        string Notes { get; }
    }
}