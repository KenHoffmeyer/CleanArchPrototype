﻿namespace Bpc.Contracts.Users
{
    public interface IUser
    {
        bool CanCreateBpc { get; }
        int Id { get; }
        string Name { get; }
    }
}