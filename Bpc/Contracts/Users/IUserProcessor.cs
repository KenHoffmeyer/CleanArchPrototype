﻿using System.Collections.Generic;

namespace Bpc.Contracts.Users
{
    public interface IUserProcessor
    {
        IEnumerable<IUser> GetUsers();
    }
}