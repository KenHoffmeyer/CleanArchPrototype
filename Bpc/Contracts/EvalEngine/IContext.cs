﻿using System.Collections;

namespace Bpc.Contracts.EvalEngine
{
    public interface IContext : IDictionary
    {
        T Get<T>(object key);
    }
}