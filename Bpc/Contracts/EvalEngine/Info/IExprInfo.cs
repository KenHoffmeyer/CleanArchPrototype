﻿using System;

namespace Bpc.Contracts.EvalEngine.Info
{
    /// <summary>
    /// Represents an expression that end-user would see/configure and the DB would store
    /// </summary>
    public interface IExprInfo
    {
        Type ResultType { get; }
    }

    /// <summary>
    /// Generic version of <see cref="IExprInfo"/>
    /// </summary>
    public interface IExprInfo<T> : IExprInfo
    {
    }
}