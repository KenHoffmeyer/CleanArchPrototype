﻿namespace Bpc.Contracts.EvalEngine.Info
{
    public interface IConditionInfo
    {
        IExprInfo<bool> Expr { get; set; }
    }
}
