﻿using Bpc.Contracts.EvalEngine.Expr;

namespace Bpc.Contracts.EvalEngine
{
    public interface ICondition : IExpr<bool>
    {
        IExpr<bool> Expr { get; }
    }
}
