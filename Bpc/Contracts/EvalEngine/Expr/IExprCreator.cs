﻿using Bpc.Contracts.EvalEngine.Info;
using Bpc.EvalEngine.Expr;

namespace Bpc.Contracts.EvalEngine.Expr
{
    /// <summary>
    /// Creates a specific <see cref="IExpr"/>
    /// </summary>
    public interface IExprCreator<T>
    {
        bool CanHandle(IExprInfo<T> info);
        IExpr<T> Create(IExprInfo<T> info);
    }
}