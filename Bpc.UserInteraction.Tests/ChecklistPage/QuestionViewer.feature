﻿Feature: QuestionViewer
  The question viewer shows questions
  It lets users answer the questions

Background: 
 Given a user with checklist that has questions
   And a checklist viewer with the user

Scenario: shows questions
  Then show each question

Scenario: answer question
 Given a question in the checklist
  When I answer "Yes"
  Then the "Yes" answer should be recorded
   And the notes entry is not active

Scenario: answer question with "No"
 Given a question in the checklist
  When I answer "No"
  Then the "No" answer should be recorded
   And the notes entry is active

Scenario: fill in notes
 Given a question in the checklist
   And I answer "No"
  When I enter a note
  Then the note should be recorded


