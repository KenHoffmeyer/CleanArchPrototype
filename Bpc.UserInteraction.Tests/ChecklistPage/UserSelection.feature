﻿Feature: User and client selection
  When the page loads, the user selector should be initialized & shown.
  When a user is selected, the client selector should be initialized & shown.
  When a user & a client are selected, the checklist viewer should be shown.

Scenario: Active user selected, client selector is shown
  Given the checklist page
    And there is no selected user
  When I select a BPC user
  Then a user is selected
   And the checklist viewer is shown