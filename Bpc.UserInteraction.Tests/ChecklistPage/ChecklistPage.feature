﻿Feature: CheckListPage
  The checklist page contains user and client selectors, and a checklist viewer.
  
Scenario: Page load
  Given the checklist page
    And there is no selected user
  When the page loads
  Then the user selector is active
   And the checklist viewer is inactive
   And the user selector contains all users

Scenario: A user is added
  Given the checklist page
    And the page already loads
   When a user is added to the system
   Then the user selector contains the new user

Scenario: A client is added
  Given the checklist page
    And the page already loads
	And a user is selected
   When a client for the user is added to the system
   Then the client selector contains the new client
