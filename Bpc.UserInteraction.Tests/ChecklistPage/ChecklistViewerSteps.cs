﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bpc.UserInteraction.Checklist;
using Bpc.UserInteraction.ChecklistPage;
using Bpc.UserInteraction.Common.SelectedItem;
using Bpc.UserInteraction.Contracts.Checklist;
using Bpc.UserInteraction.Contracts.ChecklistPage;
using Bpc.UserInteraction.Contracts.Common;
using Bpc.UserInteraction.Tests.Common;
using Bpc.UserInteraction.Users;
using FakeItEasy;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Bpc.UserInteraction.Tests.ChecklistPage
{
    [Binding]
    internal class ChecklistViewerSteps
    {
        private IChecklistViewerLogic _target;
        private IChecklistLogic _checklistLogicMock;
        private ISelectedItemBag<IUser> _selectedItemBag;
        private IChecklist _checklistMock;
        private IUser _userMock;

        [Given(@"the checklist viewer")]
        public void GivenTheChecklistViewer()
        {
            _selectedItemBag = new SelectedItemBag<IUser>();
            _checklistLogicMock = A.Fake<IChecklistLogic>();
            _target = new ChecklistViewerLogic(_checklistLogicMock, _selectedItemBag);
        }

        [Given(@"there is a selected BPC user")]
        public void GivenBpcUser()
        {
            _userMock = new UserMock(0, "BPC user", true);
        }

        [Given(@"there is (.*) checklist for the user")]
        public void GivenThereIsAChecklistForTheUser(string aOrNo)
        {
            if (aOrNo.Equals("a", StringComparison.CurrentCultureIgnoreCase))
            {
                _checklistMock = new Checklist.Checklist(0, new[] 
                {
                    new Question(0, "Question 1"),
                    new Question(1, "Question 2")
                });

                A.CallTo(() => _checklistLogicMock.GetChecklistForUser(_userMock))
                    .Returns(_checklistMock);
            }
            else
            {
                _checklistMock = null;

                A.CallTo(() => _checklistLogicMock.GetChecklistForUser(_userMock))
                    .ReturnsLazily(() => _checklistMock);
            }
        }

        [When(@"the checklist viewer loads")]
        public void WhenTheChecklistViewerLoads()
        {
            // changing user triggers a loading
            _selectedItemBag.SelectedItem = _userMock;
        }

        [Then(@"the question list is ""(.*)""")]
        public void ThenTheQuestionListIs(string shownOrHidden)
        {
            A.CallTo(() => _checklistLogicMock.GetChecklistForUser(_selectedItemBag.SelectedItem))
                .MustHaveHappened(Repeated.Exactly.Once);
            var shouldBeShown = shownOrHidden.Equals("shown", StringComparison.CurrentCultureIgnoreCase);
            Assert.AreEqual(shouldBeShown, _target.IsQuestionListVisible);
        }

        [Then(@"the option to create a checklist is ""(.*)""")]
        public void ThenTheOptionToCreateAChecklistIs(string shownOrHidden)
        {
            var shouldBeShown = shownOrHidden.Equals("shown", StringComparison.CurrentCultureIgnoreCase);
            Assert.AreEqual(shouldBeShown, _target.IsInCreateCommandOptionMode);
        }

        [When(@"the user creates a checklist")]
        public void WhenTheUserCreatesAChecklist()
        {
            ScenarioContext.Current.Pending();
        }

        
    }
}
