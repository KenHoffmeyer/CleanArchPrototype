﻿Feature: ChecklistViewer
  Checklist viewer shows the checklist for a given user and client
  If there is no checklist, it shows an option to create one
  User can answer checklist in the viewer

Background: 
	Given the checklist viewer
	  And there is a selected BPC user

Scenario: A user with checklist can see his checklist, questions and answers
	Given there is a checklist for the user
	 When the checklist viewer loads
	 Then the question list is "shown"

Scenario: Given a user but no checklist, show the option to create
  Given there is no checklist for the user
  When the checklist viewer loads
  Then the question list is "hidden"
   And the option to create a checklist is "shown"

Scenario: when a user create a checklist, show it
  Given there is no checklist for the user
  When the user creates a checklist
  Then the question list is "shown"
   And the option to create a checklist is "hidden"

