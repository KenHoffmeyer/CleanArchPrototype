﻿using System;
using System.Collections.Generic;
using Autofac;
using AutoMapper;
using Bpc.UserInteraction;
using Client.BpcServiceReference;
using Client.ChecklistPage;
using Client.Common;
using Client.Helpers;

namespace Client
{
    public class ClientAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterModule<BpcUserInteractionAutofacModule>();
            builder.RegisterModule<ClientChecklistPageAutofacModule>();
            builder.RegisterModule<ClientHelpersAutofacModule>();
            builder.RegisterModule<ClientCommonAutofacModule>();

            builder.Register(c => new BpcServiceClient()).As<IBpcService>();

            RegisterAutoMapper(builder);
            RegisterMainWindow(builder);
        }

        private static void RegisterAutoMapper(ContainerBuilder builder)
        {
            builder.Register(c =>
                {
                    var profiles = c.Resolve<IEnumerable<Profile>>();
                    var container = c.Resolve<IComponentContext>();
                    var mapperConfig = new MapperConfiguration(cfg =>
                    {
                        foreach (var profile in profiles)
                        {
                            cfg.AddProfile(profile);
                        }
                        cfg.ConstructServicesUsing(container.Resolve);
                    });

                    //typeToResolve =>
                    //{
                    //    var resolved = container.Resolve(typeToResolve);
                    //    return resolved;
                    //}
                    

                    return mapperConfig;
                })
                .As<IConfigurationProvider>()
                .InstancePerLifetimeScope();

            builder.Register(c =>
            {
                var config = c.Resolve<IConfigurationProvider>();
                var container = c.Resolve<IComponentContext>();
                var output = config.CreateMapper();
                
                return output;
            }).AsImplementedInterfaces();
        }

        private static void RegisterMainWindow(ContainerBuilder builder)
        {
            builder.RegisterType<MainWindow>()
                .OnActivating(args =>
                {
                    var mainWindow = args.Instance;
                    var container = args.Context;

                    mainWindow.Resources.Add("Resolver", container.Resolve<IResolver>());
                    mainWindow.Content = container.Resolve<ChecklistPage.ChecklistPage>();
                })
                .InstancePerLifetimeScope();
        }
    }
}
