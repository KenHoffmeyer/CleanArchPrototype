﻿using System.Windows;
using Autofac;

namespace Client
{
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var container = InitializeContainer();

            var mainWindow = container.Resolve<MainWindow>();
            mainWindow.Show();

        }

        private IContainer InitializeContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<ClientAutofacModule>();

            return builder.Build();
        }
    }
}
