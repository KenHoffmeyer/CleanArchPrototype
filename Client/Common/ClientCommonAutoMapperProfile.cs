﻿using System;
using System.Collections.Generic;
using Autofac;
using AutoMapper;
using Bpc.UserInteraction.Checklist;
using Bpc.UserInteraction.Contracts.Checklist;
using Bpc.UserInteraction.Users;
using Client.BpcServiceReference;

namespace Client.Common
{
    class ClientCommonAutoMapperProfile : Profile
    {
        public ClientCommonAutoMapperProfile(IComponentContext container)
        {
            CreateMap<UserDto, IUser>().ConstructUsingServiceLocator();

            CreateMap<AnswerDto, IAnswer>()
                .ConstructUsing(dto =>
                {
                    var ctor = container.Resolve<Func<bool?, string, IAnswer>>();
                    return ctor(dto.Yes, dto.Notes);
                });

            CreateMap<QuestionDto, IQuestion>()
                .ConstructUsing(dto =>
                {
                    var ctor = container.Resolve<Func<int, string, IQuestion>>();
                    return ctor(dto.Order, dto.Text);
                })
                .ForMember(q => q.Answer, opt => opt.ResolveUsing((dto, q, answer, context) =>
                {
                    if (dto.Answer == null)
                    {
                        var ctor = container.Resolve<Func<bool?, string, IAnswer>>();
                        answer = ctor(null, string.Empty);
                    }
                    else if (answer == null)
                    {
                        // dto.Answer != null but answer is, fill it.
                        answer = context.Mapper.Map<IAnswer>(dto.Answer);
                    }
                    return answer;
                }));

            CreateMap<ChecklistDto, IChecklist>().ConstructUsing((dto, context) =>
            {
                var ctor = container.Resolve<Func<int, IReadOnlyList<IQuestion>, IChecklist>>();

                return ctor(dto.Id, context.Mapper.Map<IReadOnlyList<IQuestion>>(dto.Questions));
            });
        }
    }
}