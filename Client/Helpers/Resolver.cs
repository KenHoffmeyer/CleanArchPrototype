﻿using System;
using Autofac;

namespace Client.Helpers
{
    class Resolver : IResolver
    {
        private readonly IComponentContext _container;

        public Resolver(IComponentContext container)
        {
            _container = container;
        }

        public T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public object Resolve(Type type)
        {
            return _container.Resolve(type);
        }
    }
}
