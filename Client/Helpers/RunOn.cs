﻿using System;
using System.Windows;

namespace Client.Helpers
{
    static class RunOn
    {
        public static void MainThread(Action action)
        {
            var dispatcher = Application.Current?.Dispatcher;
            if (dispatcher == null)
            {
                action();
            }
            else
            {
                dispatcher.Invoke(action);
            }
        }
    }
}
