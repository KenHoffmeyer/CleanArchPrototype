﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using AutoMapper.Mappers;
using Client.ChecklistPage;

namespace Client.Helpers.Xaml
{
    public static class ViewModel
    {
        public static readonly DependencyProperty TypeProperty = DependencyProperty.RegisterAttached(
            "Type", typeof(Type), typeof(ViewModel), new PropertyMetadata(default(Type), OnViewModelTypeChanged));

        private static void OnViewModelTypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as FrameworkElement;

            if (control != null)
            {
                var viewModelType = e.NewValue as Type;
                if (viewModelType != null)
                {
                    // we need the control to be initialized with a parent before we can find the resolver.
                    control.Loaded += (sender, args) =>
                    {
                        var resolver = control.TryFindResource("Resolver") as IResolver;

                        if (resolver != null)
                        {
                            var vm = resolver.Resolve(viewModelType);
                            control.DataContext = vm;
                        }
                    };
                }
            }
        }

        public static void SetType(DependencyObject element, Type value)
        {
            element.SetValue(TypeProperty, value);
        }

        public static Type GetType(DependencyObject element)
        {
            return (Type)element.GetValue(TypeProperty);
        }
    }
}
