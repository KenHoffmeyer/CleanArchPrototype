﻿using Autofac;

namespace Client.Helpers
{
    class ClientHelpersAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<Resolver>().AsImplementedInterfaces().InstancePerLifetimeScope();
        }
    }
}
