﻿using Autofac;
using AutoMapper;

namespace Client.ChecklistPage
{
    class ClientChecklistPageAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<ClientChecklistPageAutoMapperProfile>().As<Profile>();

            builder.RegisterType<ChecklistPage>().InstancePerLifetimeScope();
            builder.RegisterType<ChecklistPageVm>().InstancePerLifetimeScope();
            builder.RegisterType<ChecklistViewerVm>().InstancePerLifetimeScope();
        }
    }
}
