﻿using System.Collections.ObjectModel;
using AutoMapper;
using Bpc.UserInteraction.ChecklistPage;
using Bpc.UserInteraction.Contracts.ChecklistPage;
using Client.Helpers;
using PropertyChanged;

namespace Client.ChecklistPage
{
    [ImplementPropertyChanged]
    class ChecklistViewerVm
    {
        private readonly IChecklistViewerLogic _checklistViewerLogic;
        private readonly IMapper _mapper;

        public string Title { get; set; }
        

        public ObservableCollection<QuestionVm> Questions { get; set; }

        public ChecklistViewerVm(IChecklistViewerLogic checklistViewerLogic, IMapper mapper)
        {
            _checklistViewerLogic = checklistViewerLogic;
            _mapper = mapper;
            _checklistViewerLogic.PropertyChanged += (sender, args) => SyncWithLogic();

            SyncWithLogic();
        }


        private void SyncWithLogic()
        {
            RunOn.MainThread(() =>
            {
                _mapper.Map(_checklistViewerLogic, this);
            });
        }
    }
}
